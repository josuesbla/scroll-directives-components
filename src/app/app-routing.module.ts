import {Screen1Component} from './screens/screen1/screen1.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {Screen2Component} from './screens/screen2/screen2.component';

const routes: Routes = [
  {
    path: 'screen-1',
    component: Screen1Component,
  },
  {
    path: 'screen-2',
    component: Screen2Component,
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }

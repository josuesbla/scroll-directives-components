import { Injectable } from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import { distinctUntilChanged, map, scan } from 'rxjs/operators';
import { ViewportRuler} from '@angular/cdk/overlay';

export interface ScrollAction {
  prev?: ClientRect;
  current: ClientRect;
  headHeight: number;
  delta: number;
}

export const HEADER_MAX_HEIGHT = 300;
export const HEADER_MIN_HEIGHT = 100;

const SCROLL_ACTION_INIT = {
  headHeight: HEADER_MAX_HEIGHT,
  delta: 0,
  current: {
    bottom: 0,
    height: 0,
    left: 0,
    right: 0,
    top: 0,
    width: 0,
  }
};

const getScrollAction = (prev: ScrollAction, current: ClientRect): ScrollAction => {
  return {
    headHeight: Math.max(HEADER_MAX_HEIGHT - current.top, HEADER_MIN_HEIGHT),
    prev: prev.current,
    current: current,
    delta: current.top - prev.current.top,
  };
};

@Injectable()
export class ScrollHandlerService {
  public scroll$: Observable<ScrollAction>;
  public sidebarHeight$: BehaviorSubject<number>;

  constructor (
    private _scroll: ScrollDispatcher,
    private _viewportRuler: ViewportRuler,
  ) {
    this.sidebarHeight$ = new BehaviorSubject<number>(0);
    this.scroll$ =
      this._scroll.scrolled()
        .pipe(
          map(() => this._viewportRuler.getViewportRect()),
          distinctUntilChanged(),
          scan(getScrollAction, SCROLL_ACTION_INIT),
        );
  }

}

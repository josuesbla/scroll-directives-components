import {Component} from '@angular/core';

@Component({
  selector: 'app-screen1',
  template: `
    <div>Screen 1</div>
  `,
})
export class Screen1Component {
  constructor() {}
}

import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {HEADER_MAX_HEIGHT, ScrollAction, ScrollHandlerService} from '../../services/scroll-handler.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements AfterViewInit {

  private sideBarPosition = HEADER_MAX_HEIGHT;
  private sideBarHeight: number;

  @ViewChild('sidebar') menuInnerElement: ElementRef;

  constructor(
    private scrollHandler: ScrollHandlerService,
    private renderer: Renderer2,
    private element: ElementRef,
  ) {}

  ngAfterViewInit() {
    this.sideBarHeight = this.menuInnerElement.nativeElement.getBoundingClientRect().height;
    this.scrollHandler.sidebarHeight$.next(this.sideBarHeight);
    this.transform();
    this.scrollHandler.scroll$.subscribe((scroll: ScrollAction) => {
      this.sideBarPosition = this.getSideBarPosition(scroll);
      this.transform();
    });
  }

  private transform() {
    this.renderer.setStyle(
      this.element.nativeElement,
      'transform',
      `translateY(${this.sideBarPosition}px)`,
    );
  }

  private getSideBarPosition = (scroll: ScrollAction) => {
    const sideBarScrollSpace = this.sideBarHeight - scroll.current.height;
    if (scroll.delta > 0) {
      if (scroll.current.top <= 200) {
        return scroll.headHeight;
      }

      if (sideBarScrollSpace < -100) {
        return 100;
      }

      return Math.max(-sideBarScrollSpace, (scroll.headHeight + 200) - scroll.current.top);
    }

    if (scroll.delta < 0) {
      if (scroll.current.top <= 200) {
        return scroll.headHeight;
      } else {
        return Math.min(this.sideBarPosition - scroll.delta, scroll.headHeight);
      }
    }
  }

}

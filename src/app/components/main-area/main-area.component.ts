import {
  AfterViewInit,
  Component,
  ElementRef,
  Renderer2,
  ViewChild
} from '@angular/core';
import {ScrollHandlerService} from '../../services/scroll-handler.service';
import {NavigationEnd, Router} from '@angular/router';
import {distinctUntilChanged, take} from 'rxjs/operators';

@Component({
  selector: 'app-main-area',
  templateUrl: './main-area.component.html',
  styleUrls: ['./main-area.component.css']
})
export class MainAreaComponent implements AfterViewInit {

  @ViewChild('fakeScroll') fakeScroll: ElementRef;
  @ViewChild('main') main: ElementRef;

  constructor(
    private scrollHandler: ScrollHandlerService,
    private renderer: Renderer2,
    private element: ElementRef,
    private router: Router,
  ) {}

  ngAfterViewInit() {
    this.router.events.subscribe((val) => {
     if (val instanceof NavigationEnd) {
       this.scrollHandler.sidebarHeight$
         .pipe(
           distinctUntilChanged(),
           take(1),
         ).subscribe((height) => {
         const mainHeight = this.main.nativeElement.getBoundingClientRect().height;
         this.renderer.setStyle(
           this.fakeScroll.nativeElement,
           'height',
           `${height > mainHeight ? height - mainHeight : 0}px`
         );
       });
     }
    });
  }
}

import { Component, ElementRef, Renderer2 } from '@angular/core';
import {ScrollHandlerService, ScrollAction, HEADER_MAX_HEIGHT} from '../../services/scroll-handler.service';

@Component({
  selector: 'app-nav-header',
  templateUrl: './nav-header.component.html',
  styleUrls: ['./nav-header.component.scss']
})
export class NavHeaderComponent {

  constructor(
    private scrollHandler: ScrollHandlerService,
    private renderer: Renderer2,
    private element: ElementRef,
  ) {
    this.renderer.setStyle(
      this.element.nativeElement,
      'height',
      `${HEADER_MAX_HEIGHT}px`,
    );
    this.scrollHandler.scroll$.subscribe((scroll: ScrollAction) => {
      this.renderer.setStyle(
        this.element.nativeElement,
        'height',
        `${scroll.headHeight}px`,
      );
    });
  }
}

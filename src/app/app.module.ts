import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ScrollHandlerService} from './services/scroll-handler.service';
import {NavHeaderComponent} from './components/nav-header/nav-header.component';
import {Screen1Component} from './screens/screen1/screen1.component';
import {AppRoutingModule} from './app-routing.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderDirective } from './directives/header.directive';
import {Screen2Component} from './screens/screen2/screen2.component';
import { MainAreaComponent } from './components/main-area/main-area.component';
import { StickyDirective } from './directives/sticky.directive';
import { StickyAreaDirective } from './directives/sticky-area.directive';

@NgModule({
  declarations: [
    AppComponent,
    NavHeaderComponent,
    Screen1Component,
    Screen2Component,
    SidebarComponent,
    HeaderDirective,
    MainAreaComponent,
    StickyDirective,
    StickyAreaDirective,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule
  ],
  providers: [ ScrollHandlerService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

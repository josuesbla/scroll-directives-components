import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';
import {HEADER_MAX_HEIGHT, ScrollAction, ScrollHandlerService} from '../services/scroll-handler.service';

@Directive({
  selector: '[appHeader]'
})
export class HeaderDirective implements OnInit {

  constructor(
    private scrollHandler: ScrollHandlerService,
    private renderer: Renderer2,
    private element: ElementRef,
  ) {}

  ngOnInit() {
    this.renderer.addClass(this.element.nativeElement, 'head');
    this.renderer.setStyle(
      this.element.nativeElement,
      'height',
      `${HEADER_MAX_HEIGHT}px`,
    );
    this.scrollHandler.scroll$.subscribe((scroll: ScrollAction) => {
      this.renderer.setStyle(
        this.element.nativeElement,
        'height',
        `${scroll.headHeight}px`,
      );
    });
  }
}

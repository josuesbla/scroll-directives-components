import {AfterContentInit, ContentChildren, Directive, QueryList} from '@angular/core';
import {StickyDirective} from './sticky.directive';

@Directive({
  selector: '[appStickyArea]'
})
export class StickyAreaDirective implements AfterContentInit {

  private readonly TOP_OFFSET: number;

  @ContentChildren(StickyDirective, {descendants: true}) stickyDirectives: QueryList<StickyDirective>;

  constructor() {
    this.TOP_OFFSET =
      /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor) ?
        100 : -200;
  }

  ngAfterContentInit() {
    this.stickyDirectives.toArray().forEach((directive, index) => {
      if (index === 0) {
        directive.setTop(this.TOP_OFFSET);
      } else {
        directive.setTop(this.TOP_OFFSET + this.stickyDirectives.toArray()[(index - 1)].element.nativeElement.offsetHeight);
      }
    });
  }

}

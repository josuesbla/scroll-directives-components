import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appSticky]'
})
export class StickyDirective implements OnInit {

  constructor(
    private renderer: Renderer2,
    public element: ElementRef,
  ) {}

  ngOnInit() {
    this.renderer.addClass(
      this.element.nativeElement,
      'sticky',
    );
  }

  public setTop(top: number) {
    this.renderer.setStyle(
      this.element.nativeElement,
      'top',
      `${top}px`
    );
  }
}
